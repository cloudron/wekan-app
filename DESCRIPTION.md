### Wekan is a collaborative kanban board application

Whether you’re maintaining a personal todo list, planning your holidays
with some friends, or working in a team on your next revolutionary idea,
Kanban boards are an unbeatable tool to keep your things organized.
They give you a visual overview of the current state of your project,
and make you productive by allowing you to focus on the few items that
matter the most.

Wekan supports most features you would expect of it including a real-time
user interface, cards comments, member assignations, customizable labels,
filtered views, and more.
