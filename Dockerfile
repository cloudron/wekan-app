FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg /tmp/build
WORKDIR /app/code

# https://github.com/docker/hub-feedback/issues/727#issuecomment-299533372
# https://github.com/meteor/meteor/issues/5762
# https://github.com/moby/moby/issues/19647
RUN apt-get update && apt-get install -y --no-install-recommends libarchive-tools && rm -rf /var/cache/apt /var/lib/apt/lists

# https://github.com/wekan/wekan/blob/master/Dockerfile
ARG METEOR_VERSION=2.14
ARG NODEJS_VERSION=14.21.3

# wekan requires at least node 10
RUN mkdir -p /usr/local/node-${NODEJS_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODEJS_VERSION}
ENV PATH /usr/local/node-${NODEJS_VERSION}/bin:$PATH

RUN chown -R cloudron:cloudron /app/code /tmp
USER cloudron

RUN export tar='bsdtar'
RUN curl https://install.meteor.com/?release=${METEOR_VERSION} | sh
ENV PATH /home/cloudron/.meteor:$PATH

# renovate: datasource=github-releases depName=wekan/wekan versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.?(?<patch>\d+)?$ extractVersion=^v(?<version>.+)$
ARG WEKAN_VERSION=7.83

# a meteor bundle at the end of the day only produces a node tarball, so we have to npm install on it.
# Firefox Android fix https://github.com/wekan/wekan/commit/1235363465b824d26129d4aa74a4445f362c1a73
# See upstream rebuild-wekan.sh
RUN curl -L https://github.com/wekan/wekan/archive/v${WEKAN_VERSION}.tar.gz | tar -xz --strip-components 1 -f - -C /tmp/build && \
    cd /tmp/build && \
    meteor npm install && \
    meteor build /app/code --directory --platforms=web.browser && \
    cd /app/code/bundle/programs/server/ && npm install && \
    rm -rf /app/code/bundle/programs/web.browser.legacy && \
    rm -rf /home/cloudron/.meteor /tmp/*

WORKDIR /app/code

COPY start.sh /app/pkg/

USER root
CMD [ "/app/pkg/start.sh" ]
