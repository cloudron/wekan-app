#!/bin/bash

set -eu

cd /app/code

# Note : we do not use env vars because setting env var does not update existing value
mongo_cli="mongosh ${CLOUDRON_MONGODB_HOST}:${CLOUDRON_MONGODB_PORT}/${CLOUDRON_MONGODB_DATABASE} -u ${CLOUDRON_MONGODB_USERNAME} -p ${CLOUDRON_MONGODB_PASSWORD} --quiet"

disable_registration() {
    registration_disabled=$(${mongo_cli} --eval "db.settings.findOne({ disableRegistration: { \$exists: true } })")
    echo "=> registration status is ${registration_disabled}"
    if [[ ${registration_disabled} == "null" ]]; then
        echo "=> Disabling registration"
        ${mongo_cli} --eval "db.settings.update({}, { \$set: { disableRegistration: true }}, { upsert: true })"
        ${mongo_cli} --eval "db.settings.update({}, { \$set: { displayAuthenticationMethod: true }}, { upsert: true })"
    fi
}

export MONGO_URL="${CLOUDRON_MONGODB_URL}"
export ROOT_URL="${CLOUDRON_APP_ORIGIN}"
export MAIL_URL="smtp://${CLOUDRON_MAIL_SMTP_USERNAME}:${CLOUDRON_MAIL_SMTP_PASSWORD}@${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}?ignoreTLS=true&tls={rejectUnauthorized:false}&secure=false"
export MAIL_FROM="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Wekan} ${CLOUDRON_MAIL_FROM}"
export PORT=3000
export INTERNAL_LOG_LEVEL=info

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Setting up OpenID integration"
    export PASSWORD_LOGIN_ENABLED=false

    export OAUTH2_ENABLED=true
    export OAUTH2_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
    export OAUTH2_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
    export OAUTH2_SERVER_URL="${CLOUDRON_OIDC_ISSUER}"
    export OAUTH2_AUTH_ENDPOINT="/auth"
    export OAUTH2_USERINFO_ENDPOINT="/me"
    export OAUTH2_TOKEN_ENDPOINT="/token"
    export OAUTH2_REQUEST_PERMISSIONS="openid profile email"
    export OAUTH2_ID_MAP="sub"
    export OAUTH2_USERNAME_MAP="sub"
    export OAUTH2_FULLNAME_MAP="name"
    export OAUTH2_EMAIL_MAP="email"
    export OAUTH2_LOGIN_STYLE="redirect"

    export OIDC_REDIRECTION_ENABLED=false
else
    export PASSWORD_LOGIN_ENABLED=true
fi

# explicitly disable registration since it is true by default
# if [[ ! -f /app/data/.initialized ]]; then
#     disable_registration
# fi

# https://github.com/wekan/wekan/issues/1815 (required for board export)
export WITH_API=true

[[ ! -f /app/data/env ]] && echo -e "# Add custom environment variables in this file\nexport NOTIFY_DUE_AT_HOUR_OF_DAY=8\nexport NOTIFY_DUE_DAYS_BEFORE_AND_AFTER=2,0\n\n" > /app/data/env
source /app/data/env

touch /app/data/.initialized
chown -R cloudron:cloudron /app/data

export WRITABLE_PATH="/app/data/"

echo "=> Starting wekan"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/bundle/main.js
