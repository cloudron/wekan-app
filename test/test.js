#!/usr/bin/env node

'use strict';

/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const BOARD_NAME = 'test board cloudron ' + Math.floor((Math.random() * 100) + 1);
    const LIST_NAME = 'test list cloudron';
    const CARD_CONTENT = 'test card content cloudron';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(identifier, password) {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn + '/sign-in');
        await waitForElement(By.id('at-oidc'));
        await browser.findElement(By.id('at-oidc')).click();

        await browser.sleep(2000);

        const originalWindowHandle = await browser.getWindowHandle();
        const allWindowHandles = await browser.getAllWindowHandles();
        const newWindowHandle = allWindowHandles.filter((h) => h !== originalWindowHandle).join();

        await browser.switchTo().window(newWindowHandle);

        // oidc login
        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(identifier);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.id('loginSubmitButton')).click();

        await browser.switchTo().window(originalWindowHandle);

        // wait for login model sync
        await browser.sleep(3000);

        await waitForElement(By.className('js-add-board'));
    }

    async function loginWithOidcSession() {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn + '/sign-in');
        await waitForElement(By.id('at-oidc'));
        await browser.findElement(By.id('at-oidc')).click();

        // wait for login model sync
        await browser.sleep(3000);

        await waitForElement(By.className('js-add-board'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(3000);
        await waitForElement(By.id('header-user-bar'));
        await browser.findElement(By.id('header-user-bar')).click();
        await waitForElement(By.className('js-logout'));
        await browser.findElement(By.className('js-logout')).click();
        await waitForElement(By.xpath('//h3[text()="Sign In"]'));
    }

    async function signUp(username, email, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/sign-up');
        await waitForElement(By.name('at-field-username'));
        await browser.findElement(By.name('at-field-username')).sendKeys(username);
        await browser.findElement(By.name('at-field-email')).sendKeys(email);
        await browser.findElement(By.name('at-field-password')).sendKeys(password);
        await browser.findElement(By.name('at-field-password_again')).sendKeys(password);

        // wait for login model sync
        await browser.sleep(1000);
        await browser.findElement(By.id('at-btn')).click();
        await waitForElement(By.className('js-add-board'));
    }

    async function contentExists() {
        const tag = app.manifest.version === '3.48.0' ? 'span' : 'p';
        await browser.get('https://' + app.fqdn);
        await browser.sleep(4000);
        await waitForElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`));
        await browser.findElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`)).click();
        await waitForElement(By.xpath('//p[text()="' + CARD_CONTENT + '"]'));
    }

    async function createBoard() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(6000);
        await waitForElement(By.className('js-add-board'));
        await browser.findElement(By.className('js-add-board')).click();
        await waitForElement(By.className('js-new-board-title'));
        await browser.findElement(By.className('js-new-board-title')).sendKeys(BOARD_NAME);
        await browser.findElement(By.className('js-new-board-title')).sendKeys(Key.ENTER);
        await browser.sleep(4000);
        await waitForElement(By.xpath('//a[@title="Board View"]'));
        await browser.sleep(4000);
    }

    async function addList() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath(`//p[text()="${BOARD_NAME}"]`));
        await browser.findElement(By.xpath(`//p[text()="${BOARD_NAME}"]`)).click();
        await waitForElement(By.className('open-list-composer'));
        await browser.findElement(By.className('open-list-composer')).click();
        await waitForElement(By.className('list-name-input'));
        await browser.findElement(By.className('list-name-input')).sendKeys(LIST_NAME);
        await browser.findElement(By.className('list-name-input')).sendKeys(Key.ENTER);
        await waitForElement(By.className('open-minicard-composer'));
        await browser.sleep(4000);
    }

    async function addCard() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath(`//p[text()="${BOARD_NAME}"]`));
        await browser.findElement(By.xpath(`//p[text()="${BOARD_NAME}"]`)).click();
        await waitForElement(By.className('open-minicard-composer'));
        await browser.findElement(By.className('open-minicard-composer')).click();
        await waitForElement(By.className('minicard-composer-textarea'));
        await browser.findElement(By.className('minicard-composer-textarea')).sendKeys(CARD_CONTENT);
        await browser.findElement(By.className('minicard-composer-textarea')).sendKeys(Key.ENTER);
        await waitForElement(By.xpath('//p[text()="' + CARD_CONTENT + '"]'));
        await browser.sleep(4000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can create board', createBoard);
    it('can add list', addList);
    it('can add card', addCard);
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login with username', loginWithOidcSession);
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login with username', loginWithOidcSession);
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login with username', loginWithOidcSession);
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can register user', signUp.bind(null, process.env.USERNAME, process.env.EMAIL, process.env.PASSWORD));
    it('can logout', logout);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id io.wekan.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    // it('can login with username', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can login with username', loginWithOidcSession);
    it('can create board', createBoard);
    it('can add list', addList);
    it('can add card', addCard);
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login with username', loginWithOidcSession);
    it('board, list and card exists', contentExists);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
