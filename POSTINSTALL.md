<sso>
Cloudron users must login to Wekan before they can discover and share boards with each other.
The first user to login is made a Wekan admin.
</sso>

<nosso>
On first visit, register yourself to start using the app.
</nosso>

