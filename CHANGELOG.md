[1.0.0]
* Initial version

[1.0.1]
* Fix bug when firstime login was with username

[1.0.2]
* Update to new base image

[1.0.3]
* Use new base image 0.9.0

[1.0.4]
* Update screenshots

[1.1.0]
* Update to version 0.11.0 (fork of wekan)

[2.0.0]
* Use wekan 0.11.1-rc1 (reunited wekan with wefork)
* Update to new base image

[2.0.1]
* Fix email send

[2.1.0]
* Update meteor
* Use LDAP instead of simpleauth
* Update wekan to 0.16

[2.2.0]
* Update wekan to 0.18

[2.3.0]
* Update to upstream version 0.22

[2.4.0]
* Update to Wekan 0.23

[2.5.0]
* Update to Wekan 0.24
* Fix critical security issue where Admin Panel link available to all users (#1048)

[2.6.0]
* Update to Wekan 0.25
* Fix admin panel route for subfolder

[2.7.0]
* Update to Wekan 0.26
* Fix admin panel route for subfolder

[2.8.0]
* Update to Wekan 0.27

[2.9.0]
* Update to Wekan 0.28

[2.10.0]
* Update to Wekan 0.29

[2.11.0]
* Update to Wekan 0.30

[2.12.0]
* Update to Wekan 0.31

[2.13.0]
* Update to Wekan 0.32
* Fixes security issue where files were accessible without authentication (#1105)

[2.14.0]
* Update to Wekan 0.33
* https://github.com/wekan/wekan/blob/devel/CHANGELOG.md#v033-2017-08-29-wekan-release

[2.15.0]
* Update to Wekan 0.34

[2.16.0]
* Update to Wekan 0.35

[2.17.0]
* Update to Wekan 0.36

[2.18.0]
* Update to Wekan 0.37
* Adds ability to copy a card within one board

[2.19.0]
* Update to Wekan 0.38

[2.20.0]
* Update to Wekan 0.39

[2.21.0]
* Update to Wekan 0.43

[2.22.0]
* Update to Wekan 0.47
* [Slider for Comments only in activity feed](https://github.com/wekan/wekan/issues/1247)
* [Site Wide Announcement](https://github.com/wekan/wekan/pull/1260).
* [Use theme color for Slider for Comments only](https://github.com/wekan/wekan/pull/1275).
* SECURITY FIX: [Meteor allow/deny](https://blog.meteor.com/meteor-allow-deny-vulnerability-disclosure-baf398f47b25) fixed
* [Fix: Slider for Comments only does not work correctly with over 21 activities](https://github.com/wekan/wekan/pull/1247).

[2.23.0]
* Update to Wekan 0.50
* Use meteor 1.5.2.2

[2.24.0]
* Update to Wekan 0.51

[2.25.0]
* Update to Wekan 0.52

[2.26.0]
* Update to Wekan 0.54
* Introduces soft limits for WIP cards

[2.26.1]
* Fixes user login to be case insensitive

[2.27.0]
* Update to Wekan 0.55
* Fix crash when trying to login with invalid user id

[2.28.0]
* Update Wekan to 0.56

[2.29.0]
* Update Wekan to 0.60

[2.30.0]
* Update Wekan to 0.61

[2.31.0]
* Update Wekan to 0.62

[2.32.0]
* Update Wekan to 0.63

[2.33.0]
* Update Wekan to 0.67

[2.34.0]
* Update Wekan to 0.68

[2.35.0]
* Update Wekan to 0.69

[2.36.0]
* Update Wekan to 0.71

[2.37.0]
* Update Wekan to 0.72

[2.38.0]
* Update Wekan to 0.73

[2.39.0]
* Update Wekan to 0.75

[2.40.0]
* Update Wekan to 0.76

[2.41.0]
* Update Wekan to 0.77
* Search from card titles and descriptions on this board.
* Add Bulgarian language.
* card-shadow no longer covered the page if you scroll down.

[2.42.0]
* Update Wekan to 0.78
* Swimlanes can now be reordered
* List view is now the default board view

[2.43.0]
* Update Wekan to 0.79
* Adds checklist sorting
* Fixes drag'n'drop in lists view

[2.43.1]
* Update Wekan to 0.80

[2.44.0]
* Update Wekan to 0.81

[2.45.0]
* Update Wekan to 0.82

[2.46.0]
* Update Wekan to 0.83

[2.47.0]
* Update Wekan to 0.85

[2.48.0]
* Update Wekan to 0.88

[2.49.0]
* Update Wekan to 0.89

[2.50.0]
* Update Wekan to 0.95

[3.0.0]
* Update Wekan to 1.00

[3.0.1]
* Update Wekan to 1.01

[3.0.2]
* Update Wekan to 1.02

[3.0.3]
* Update Wekan to 1.03

[3.0.4]
* Update Wekan to 1.04

[3.0.5]
* Update Wekan to 1.07

[3.0.6]
* Update Wekan to 1.11

[3.1.0]
* Update Wekan to 1.19

[3.2.0]
* Update Wekan to 1.19

[3.3.0]
* Update Wekan to 1.21

[3.3.1]
* Update Wekan to 1.23

[3.4.0]
* Update Wekan to 1.29

[3.5.0]
* Update Wekan to 1.35

[3.6.0]
* Update Wekan to 1.45
* Fix board export
* Enable use of API

[3.7.0]
* Update Wekan to 1.47

[3.8.0]
* Update Wekan to 1.49

[3.9.0]
* Update Wekan to 1.50.3

[3.10.0]
* Update Wekan to 1.53

[3.11.0]
* Update Wekan to 1.54

[3.12.0]
* Update Wekan to 1.57
* Fix issue where [email is being sent for every change](https://github.com/wekan/wekan/issues/1952)

[3.12.1]
* Update Wekan to 1.65
* Fix issue where a new user is unable to sign in

[3.13.0]
* Update Wekan to 1.69

[3.14.0]
* Update Wekan to 1.70

[3.15.0]
* Update Wekan to 1.76

[3.16.0]
* Update Wekan to 1.78

[3.17.0]
* Update Wekan to 1.79

[3.18.0]
* Update Wekan to 1.80

[3.19.0]
* Update Wekan to 1.85

[3.20.0]
* Update Wekan to 1.86

[3.21.0]
* Update Wekan to 2.01

[3.22.0]
* Update Wekan to 2.02

[3.23.0]
* Update Wekan to 2.07

[3.24.0]
* Update Wekan to 2.09

[3.25.0]
* Update Wekan to 2.10

[3.26.0]
* Update Wekan to 2.13

[3.27.0]
* Update Wekan to 2.17

[3.28.0]
* Update Wekan to 2.20

[3.29.0]
* Update Wekan to 2.22

[3.30.0]
* Update Wekan to 2.23

[3.31.0]
* Update Wekan to 2.25

[3.32.0]
* Update Wekan to 2.27

[3.33.0]
* Update Wekan to 2.31

[3.34.0]
* Update Wekan to 2.35

[3.35.0]
* Update Wekan to 2.37

[3.36.0]
* Update Wekan to 2.43

[3.37.0]
* Update Wekan to 2.47

[3.38.0]
* Update Wekan to 2.48

[3.39.0]
* Update Wekan to 2.51

[3.40.0]
* Update Wekan to 2.52

[3.41.0]
* Update Wekan to 2.53

[3.42.0]
* Update Wekan to 2.54

[3.43.0]
* Update Wekan to 2.57

[3.44.0]
* Update Wekan to 2.59

[3.45.0]
* Update Wekan to 2.60

[3.46.0]
* Update Wekan to 2.64

[3.47.0]
* Update Wekan to 2.65

[3.48.0]
* Update Wekan to 2.66

[3.49.0]
* Update Wekan to 2.74

[3.51.0]
* Update Wekan to 2.76

[3.52.0]
* Update Wekan to 2.78

[3.53.0]
* Update Wekan to 2.82
* Update manifest to v2

[3.54.0]
* Update Wekan to 2.83

[3.55.0]
* Update Wekan to 2.86

[3.56.0]
* Update Wekan to 2.90

[3.57.0]
* Update Wekan to 2.95

[3.58.0]
* Update Wekan to 2.97

[3.59.0]
* Update Wekan to 2.99

[3.60.0]
* Update Wekan to 3.01

[3.61.0]
* Update Wekan to 3.02

[3.62.0]
* Update Wekan to 3.03

[3.63.0]
* Update Wekan to 3.08

[3.64.0]
* Update Wekan to 3.11

[3.65.0]
* Update Wekan to 3.12

[3.66.0]
* Update Wekan to 3.15

[3.67.0]
* Update Wekan to 3.17

[3.68.0]
* Update Wekan to 3.20

[3.69.0]
* Update Wekan to 3.21

[3.70.0]
* Update Wekan to 3.23

[3.71.0]
* Update Wekan to 3.25

[3.72.0]
* Update Wekan to 3.35

[3.73.0]
* Update Wekan to 3.37

[3.74.0]
* Update Wekan to 3.39

[3.75.0]
* Update Wekan to 3.40

[3.76.0]
* Update Wekan to 3.42

[3.77.0]
* Update Wekan to 3.44

[3.78.0]
* Update Wekan to 3.45

[3.79.0]
* Update Wekan to 3.46

[3.80.0]
* Update Wekan to 3.49

[3.81.0]
* Update Wekan to 3.52

[3.82.0]
* Update Wekan to 3.53

[3.83.0]
* Update Wekan to 3.55

[3.84.0]
* Update Wekan to 3.56

[3.85.0]
* Update Wekan to 3.57

[3.86.0]
* Update Wekan to 3.61

[3.87.0]
* Update Wekan to 3.63

[3.88.0]
* Update Wekan to 3.69

[3.89.0]
* Update Wekan to 3.71

[3.90.0]
* Update Wekan to 3.73

[3.91.0]
* Update Wekan to 3.74

[3.92.0]
* Update Wekan to 3.75

[3.93.0]
* Update Wekan to 3.76

[3.94.0]
* Update Wekan to 3.77

[3.95.0]
* Update Wekan to 3.80

[3.96.0]
* Update Wekan to 3.83

[3.97.0]
* Update Wekan to 3.84

[3.98.0]
* Update Wekan to 3.85

[3.99.0]
* Update Wekan to 3.86

[3.100.0]
* Update Wekan to 3.87

[3.101.0]
* Update Wekan to 3.88

[3.102.0]
* Update Wekan to 3.90

[3.103.0]
* Update Wekan to 3.93

[3.104.0]
* Update Wekan to 3.95

[3.105.0]
* Update Wekan to 3.96

[3.106.0]
* Update Wekan to 3.97

[4.0.0]
* Update Wekan to 4.01
* From this release on we use the upstream LDAP module instead of a Cloudron specific
* If any account migration fails, please revert and contact support@cloudron.io

[4.0.1]
* Fix Firefox Mobile on Android

[4.0.2]
* Update Wekan to 4.02

[4.0.3]
* Update Wekan to 4.03

[4.0.4]
* Update Wekan to 4.04

[4.0.5]
* Update Wekan to 4.05

[4.0.6]
* Update Wekan to 4.07

[4.0.7]
* Update Wekan to 4.09

[4.1.0]
* Update Wekan to 4.10

[4.1.1]
* Update Wekan to 4.11

[4.1.2]
* Update Wekan to 4.12
* New default is now Swimlanes
* Use markdown in Swimlane titles

[4.1.3]
* Update Wekan to 4.13

[4.1.4]
* Update Wekan to 4.15

[4.1.5]
* Update Wekan to 4.17

[4.2.0]
* Disable registration, if you set or unset explicitly
* Add docs on how to make user an admin

[4.3.0]
* Update Wekan to 4.18
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.18)
* All logged in users are now allowed to reorder boards by dragging at All Boards page and Public Boards page.
* Fix creating user misbehaving in some special case.
* Thanks to xet7.

[4.4.0]
* Add custom environment variables in `/app/data/env`

[4.4.1]
* Update Wekan to 4.19

[4.4.2]
* Update Wekan to 4.20
* Update dependencies
* Fix changing slug on card rename.
* Add missing Wekan logo sizes for PWAs and Apps.

[4.4.3]
* Update Wekan to 4.21

[4.4.4]
* Update Wekan to 4.22
* Export to JSON and HTML .zip file, that also fixes #3216 Clone Boards not working.
* Hide CSV export until it's fixed in EdgeHTML compatible way.

[4.4.5]
* Update Wekan to 4.23
* Update vulnerable dependency elliptic that is dependency of meteor-node-stubs that is dependency of
Wekan.

[4.4.6]
* Update Wekan to 4.24

[4.4.7]
* Update Wekan to 4.25

[4.4.8]
* Update Wekan to 4.26
* Disable list formatting and converting to HTML. This fixes markdown numbering and viewing bugs.

[4.4.9]
* Update Wekan to 4.27
* Reverted incomplete fix for "Checklist + card title with starting number and point", because it disabled some markdown.
Also more fixes to GFM checklist not displayed properly.

[4.4.10]
* Update Wekan to 4.29
* Changed markdown from marked to markdown-it and added emoji support https://github.com/wekan/wekan/wiki/Emoji
* Fix card scrollbar on Windows.
* Try to fix language names.

[4.4.11]
* Update Wekan to 4.32

[4.4.12]
* Update Wekan to 4.36
* Added some CAS and SAML settings.
* Update dependencies

[4.5.0]
* Update Wekan to 4.40
* Custom Logo for Login and Top Left Corner. Optional link when clicking logo. Settings at Admin Panel / Layout.

[4.5.1]
* Update Wekan to 4.41
* At Admin Panel / Layout: Text below Custom Login Logo. Can have markdown formatting

[4.5.2]
* Update Wekan to 4.42
* Upgrade to Node.js 12.19.0

[4.5.3]
* Update Wekan to 4.43
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.43)
* Allow more than one assignee

[4.5.4]
* Update Wekan to 4.44
* Changed public board changing Swimlanes/Lists/Calendar view and changing Hide minicard label text from using cookies to using browser localStorage, to remove some errors from browser inspect console.
* Fix: Modern theme board canvas background.
* Fix: Expose moving cards on mobile to workers.
* Fix: Hide the move to another board functionality in the submenu (only from the worker) so that the worker is still constrained to a single board.

[4.6.0]
* Update Wekan to 4.45
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.45)
* Fix can not upload and download files, by changing back to Node.js 12.19.0 and adding fast-render.
* Current file storing to MongoDB code was not yet compatible with newer Node.js.

[4.6.1]
* Update Wekan to 4.47
* Fix: Use current boardId when a worker moves a card.

[4.6.2]
* Update Wekan to 4.48
* Smaller board icons to All Boards Page, and use full page width, so more board icons fit visible at once.
* Removed variable height, because different heights made it look a little unbalanced.
* Admin Panel / Settings / Layout / Custom Top Left Corner Logo Height.
* When RICHER_CARD_COMMENT_EDITOR=true, use richer editor also when editing card description.
* Removed hot-module-replacement and mdg:meteor-apm-agent.
* Fix Clone Board.

[4.6.3]
* Update Wekan to 4.49
* Sync email address from LDAP

[4.6.4]
* Update Wekan to 4.51

[4.6.5]
* Update Wekan to 4.52
* Some more small improvements to Modern Dark theme.

[4.6.6]
* Update Wekan to 4.53
* Minor improvements to Modern Dark theme.
* Added missing bottom padding to lists.

[4.7.0]
* Update Wekan to 4.54
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.45)
* Fix can not upload and download files, by changing back to Node.js 12.19.0 and adding fast-render.

[4.7.1]
* Update Wekan to 4.55
* Set minimum height to icons at All Boards page.
* Increase avatar size.
* Modern Dark theme: card details as lightbox.

[4.7.2]
* Update Wekan to 4.56
* Added date notification icons
* Sticky swimlane
* Improvements in activities design

[4.7.3]
* Update Wekan to 4.57
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.47)

[4.7.4]
* Update Wekan to 4.58
* Fix issues when duplicating board

[4.7.5]
* Update Wekan to 4.59
* Fix not all checklist items being imported/cloned

[4.7.6]
* Update Wekan to 4.60

[4.7.7]
* Update Wekan to 4.61
* Removed cookie code that is not in use.
* Allow normal user to delete checklist item.

[4.7.8]
* Update Wekan to 4.62
* Treat unknown attachment types as binary on board import/clone.
* Fix Move card from a board to another does not work anymore.
* Add some permission code, to see does it fix something.
* Fix delete board button not visible.
* Board: When removing member from board, remove also from assignees.
* Admin Panel/People:
* 1) Allow edit user that does not have email address.
* 2) When creating new user, require username and email address, and save also fullname.
* 3) Some in progress code for deleting user, that does not work correctly yet, so deleting user is not enabled yet.

[4.7.9]
* Update Wekan to 4.63
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.63)
* Fixed Remove Cover button gives JS error.

[4.8.0]
* Update Wekan to 4.66
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.66)
* Fix Mobile miniscreen: Drag handle not visible in long checklist item text.
* Fixed Drag and drop between checklists closes the card sometimes on Firefox.
* Dark theme button background fix.

[4.8.1]
* Update Wekan to 4.68
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.68)
* Checklist-Items, Drag-Drop Handle now at the left side.
* Checklist-Items, Autoresize the textarea vertically to fit the user-input.

[4.8.2]
* Update Wekan to 4.69
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.69)

[4.8.3]
* Update Wekan to 4.70
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.70)
* Added markdown and emoji to My Cards board, swimlane and list names.
* Show Admin Panel / People and Version also on mobile MiniScreen.
* Update to Nodejs 12.20.1

[4.8.4]
* Update Wekan to 4.72
* My Cards add Due Date sort
* Update to My Cards
* Cards, custom fields are displayed in alphabetic order
* Fixed Color picker of lists is empty

[4.9.0]
* Update Wekan to 4.75
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.75)
* Contains critical security fixes
* Due Cards and Broken Cards: In All Users view, fixed to show cards only from other users Public Boards. Not anymore from private boards.
* Upgrade to Meteor 1.12.1.

[4.9.1]
* Update Wekan to 4.76
* Try to allow links to onenote, mailspring and file.
* Removed wekan- from export filenames for whitelabeling.

[4.9.2]
* Update Wekan to 4.77
* Show membertype (admin, normal etc) in avatar/initials tooltip for board members.

[4.9.3]
* Update Wekan to 4.78
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.78)
* Search All Boards
* Limit amount of data in publications where possible

[4.9.4]
* Update Wekan to 4.81
* Global Search: Use translated color names.
* Use table-cell instead of inline-block in my-cards-list-wrapper CSS.
* Update dependencies.
* Fixed Linked card makes board not load when CustomField definition is undefined.
* At Search All Cards, now it's possible to click found card to open it.
* Fixed: Linked card makes board not load.

[4.9.5]
* Update Wekan to 4.83
* When copying a board, copy Custom Fields to new board.
* Upgrade to Meteor 2.0
* Fix custom field definitions duplicated on copy and move.

[4.9.6]
* Update Wekan to 4.85
* Option to add custom field to all cards.
* Added checkbox as an option to custom field create dialog.
* Display My Cards lists using inline-grid.
* Added board title link and background color to My Cards.
* Use simple border at My Cards.
* WIP Limit: Limited number of cards highlighting to true overbooking.
* Revert table-cell back to inline-block at my-cards-list-wrapper.
* Fix for search operators with uppercase letters.
* Boards.copyTitle - escape string used in regex.
* Bug fix: import regex escape function.
* Search All Boards: Added list of board, list and color names. Added operators for due, created and modified.
* Added support for clicking list titles, label names and board titles. Make some heading translatable.
* Set focus back to search phrase input after clicking a predicate. Fixed some spacing issues.
* Fixed Upper/lowercase errors in some languages due to .toLowerCase.
* Tried to fix possible prototype pollution reported by Deepcode.ai.
* Disable some logs that are not needed anymore.
* Rules not copied during board copy.

[4.9.7]
* Update Wekan to 4.87
* Added PWA related category, orientation, screenshots, maskable icon and IARC rating ID.
* Added PWA related monochrome icon.
* Move call to URL search to onRendered.

[4.9.8]
* Update Wekan to 4.90
* Create unique board names when importing.
* Added missing backtick quotes.
* Fix some bugs when importing Wekan JSON.
* Try to fix quotes in Global Search.
* Additional URL schemes: SolidWorks PDM (conisio:) and abas ERP (abasurl:).
* Mobile and Desktop have now the same Quick Access view + scrollable.
* Global Search Update.
* Added many more fields to Export to Excel, and better formatting. Does not yet have all fields.
* Repair LDAP_REJECT_UNAUTHORIZED=false CVE-2021-3309.

[4.9.9]
* Update Wekan to 4.91
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v4.91)

[4.9.10]
* Update Wekan to 4.92
* Added Createtoken API.
* Sorted archives.
* Updated dependencies.
* Don't reload page when returning default sort.
* Avatar overlaped notifications.
* Hopeful fix for i18n not working in onRendered().
* Disable some console.log code, that is only needed while developing.
* Try fix removed nonexistent document error.
* Fixed Cards and CustomFields sorted alphabetically.
* Notifications avatar overlaped at mobile view.
* Added badge for CII Best Practices.
* Added PGP public key for sending security vulnerability reports.
* Updated security report email address.

[4.9.11]
* Update Wekan to 4.93
* Add the ability to call get_user operation with username.
* Set the language on TAPi18n when user selects language.
* Fix bug in uniqueTitle.
* Fixed file permissions in build scripts.
* Red line below the avatar now correctly on FireFox.
* Notifications, enable line wrapping.

[4.10.0]
* Update Wekan to 4.94
* Update base image to version 3
* Settings, "Show cards count" now works at mobile view too.
* Fix bug in adding new users.
* Fixed Board does not load, by disabling Custom Fields sorting.

[4.10.1]
* Update Wekan to 4.96
* Add /api/boards_count endpoint for statistics.
* Added possibility to specify hours in single digits in 24 hour format.
* Added replacement from comma to dot.
* Checklistitems are now inserted always at the end of the checklist.
* Teams/Organizations: Added more code to Admin Panel for saving and editing. In Progress, does not work yet.
* Mobile View, list header is now always at top and only lists/cards view have a scroll area.
* Added ChangeLog update script.
* Helm: Made SecretEnv a secret and added default mongodb name as Wekan.
* Checklist drag handle now at the left side (same place as for the checklist items).
* Lists, show also 0 cards at column description.
* Updated Node.js to v12.20.2.
* Minicard, remove red line below member avatar icon.
* Added padding.
* Changed default behaviour for BIGEVENTS that no activity matches it.
* Modern theme: Remove font color when the card has a color.

[4.10.2]
* Update Wekan to 4.99
* Updated to Meteor 2.1
* Fixed SMTP password visible to Admin at Admin Panel by using browser inspect to see behind asterisks.
* Added sort feature for viewing of cards.
* Fix bugs with customFields in Webhooks.
* Global Search Updates.
* Admin Panel/People/People/New User: Added Initials.

[4.11.0]
* Update Wekan to 5.01
* Fixed Unable to remove old Board, reappears.
* Fix typo in activities code. Fixes can not edit Custom Field.

[4.11.1]
* Update Wekan to 5.02
* Added sort to edit card REST API.
* Add attachmentId to the Webhook data.
* Removed extra imports of Meteor.

[4.11.2]
* Update Wekan to 5.03
* Revert Removed extra imports of Meteor. Hopefully fixes email notifications and rules on old cards not working.
* Fixed Bug: Link at board title can not be edited.

[4.11.3]
* Update Wekan to 5.04
* Speed improvement: Delete presences older than one week, and add database index to presences serverId.
* Added autolinking settings in Admin Panel.
* Add custom field editing to the REST API.
* Related to custom field editing, Fixed generating API docs and Wekan Custom Fields REST API.
* Fix search on labels server error.
* Fixed Bug: inconsistent use of relative/absolute URLs.

[4.11.4]
* Update Wekan to 5.05
* Change URL scheme recognition for allowing abasurl to link.
* Fix email setup

[4.11.5]
* Update Wekan to 5.06
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.06)

[4.11.6]
* Update Wekan to 5.07
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.07)
* Fixed sort cards feature.

[4.11.7]
* Update Wekan to 5.08
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.08)

[4.11.8]
* Update Wekan to 5.13
* Fix HTTP not defined
* Added emoji picker to card description edit and card comment edit.
* Removed and disabled Summernote wysiwyg editor, package-lock.json
* Move swimlane from one board to another.
* Added translatable Move Swimlane popup title.
* REST API: Export one attachment.

[4.11.9]
* Update Wekan to 5.14
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.14)
* Clean-up Global Search, Due Cards, and My Cards.
* Require signed-in user for My Cards, Due Cards, and global search.

[4.11.10]
* Update Wekan to 5.15
* Fixed card sort reset.
* Fix bug in My Cards and Global Search.
* Fix bug in Due Cards introduced by last bug fix.
* Fixed Bug: Move Swimlane to Archive does not work anymore.

[4.11.11]
* Update Wekan to 5.17
* Fix Link dialog closes card when clicking in dialog.
* Added back Summernote editor. Removed emoji picker.

[4.12.0]
* Update Wekan to 5.20
* OpenAPI: rework the allowedValues to allow for imported variables.
* Custom Field "String Template".
* Update Admin Panel Rules report icon and add missing translations.
* Bug fix: Rules for moving from list/swimlane.
* Fixed Elements are duplicated on the view "My cards".
* Rewrite routine for building the My Cards hierarchical list.
* Use a separate publication for retrieving My Cards.
* Fixed bug with limit and skip projection.
* Popover needs to be destroyed anytime the details panel is closed.

[4.12.1]
* Update Wekan to 5.22
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.08)

[4.12.2]
* Update Wekan to 5.23

[4.12.3]
* Update Wekan to 5.23
* Copy Swimlane
* Updated dependencies

[4.12.4]
* Update Wekan to 5.24
* Swimlane in Export to Excel.
* Bugfix/Summernote on paste.
* OpenAPI: Better handle nested schemas.

[4.12.5]
* Update Wekan to 5.25
* Feature/mini card subtask count.
* Bring back Almost-Due for Start Date.

[4.12.6]
* Update Wekan to 5.26
* Fixed Non-ASCII attachment filename will crash when downloading.

[4.12.7]
* Update Wekan to 5.27
* Mermaid Diagram.
* Fix: BG color of StartDate.

[4.12.8]
* Update Wekan to 5.29
* Updated dependencies
* Excel parent card name export.
* Added updated Forgot Password page to GitHub issue template

[4.12.9]
* Update Wekan to 5.30
* Planning Poker / Scrum Poker,
* Fixed Python API example: Edit card, etc.
* Default language is still used although this one has been modified previously.
* Moved Keyboard Shortcuts from bottom to top of Sidebar.

[4.12.10]
* Update Wekan to 5.31
* Admin Panel: Edit Organizations and Teams.
* Admin Panel: Delete Organizations and Teams.
* Admin Panel Organizations/Teams: Show confirm text above delete button.
* Gantt: Retain links created between tasks. Part 1: Database changes, not active in MIT Wekan.
* Removed extra package.
* Updated dependencies.
* Now new boards do not have any labels added by default.
* Try to fix OAUTH2_LOGIN_STYLE=redirect Has No Effect.
* Try to fix: Wekan UI fails to finish import of closed Trello boards.
* Partial Fix: Vote and Planning Poker: Setting date and time now works for some languages that have ascii characters in date format.

[4.12.11]
* Update Wekan to 5.32
* Moved many button texts etc to tooltips. Added more tooltips.

[4.12.12]
* Update Wekan to 5.33
* Assigning a user to a team or an organization.
* Custom Fields stringtemplate, autofocus the last input box.

[4.12.13]
* Update Wekan to 5.34
* View and change card sort number.
* More spinners + configureable in admin panel.
* Added remaining spinner settings.
* Card Description has now the same color on view and editing.
* Fix Google SSO to access Wekan has not been working by reverting Wekan v5.31 not-working fixes to OAUTH2_LOGIN_STYLE=redirect Has No Effect.
* CustomFields were not created after adding 1 card.
* Try to fix BUG: Database error attempting to change a account.

[4.13.0]
* Update Wekan to 4.35
* Wait Spinners can now be translated
* Added Wait Spinners docs: https://github.com/wekan/wekan/wiki/Wait-Spinners .
* Maximize Card.
* Export Card to PDF. In Progress, does not work yet.

[4.13.1]
* Update Wekan to 4.36

[4.13.2]
* Update Wekan to 4.37
* Update nodejs to 12.22.2

[4.13.3]
* Update Wekan to 4.38

[4.13.4]
* Update Wekan to 4.39

[4.13.5]
* Update Wekan to 5.40

[4.13.6]
* Update Wekan to 5.41

[4.14.0]
* Update Wekan to 5.42

[4.14.1]
* Update Wekan to 5.44

[4.14.2]
* Update Wekan to 5.47

[4.14.3]
* Update Wekan to 5.48
* Update Node.js to 12.22.5
* Searchfields for members and assignees card popups.
* Fixed: Can't save user without Initials.

[4.14.4]
* Update Wekan to 5.49

[4.14.5]
* Update Wekan to 5.50
* Fix: Save user initials and fullname when a new user is created.

[4.14.6]
* Update Wekan to 5.51

[4.14.7]
* Update Wekan to 5.52

[4.14.8]
* Update Wekan to 5.54

[4.15.0]
* Update Wekan to 5.56

[4.15.1]
* Update Wekan to 5.57

[4.15.2]
* Update Wekan to 5.58

[4.15.3]
* Update Wekan to 5.59

[4.15.4]
* Update Wekan to 5.60

[4.15.5]
* Update Wekan to 5.61

[4.15.6]
* Update Wekan to 5.62

[4.15.7]
* Update Wekan to 5.63

[4.15.8]
* Update Wekan to 5.64

[4.15.9]
* Update Wekan to 5.65

[4.15.10]
* Update Wekan to 5.67

[4.16.0]
* Update Wekan to 5.68
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.68)
* Labels are now drag/drop/sortable.
* Fix labels desktop view add and delete.

[4.16.1]
* Update Wekan to 5.70
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.70)
* Fix bug related to Admin Panel teams management. Thanks to Emile840.
* Popup fixes: Archive cards, upload attachements etc. Thanks to mfilser.

[4.16.2]
* Update Wekan to 5.71
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.71)
* Updated dependencies. Thanks to developers of dependencies.
* Fix: Filter List by Card Title. Thanks to Ben0it-T.
* Add info about upgrades to GitHub issue template. Thanks to xet7.

[4.16.3]
* Update Wekan to 5.72
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.72)
* Add a possibility for non-admin users (who have an email on a given domain name in Admin Panel) to invite new users for registration. Thanks to Emile840.
* Try to fix: Filter List by Title - Hide empty lists in Swimlane view. Thanks to Ben0it-T.
* Card labels on minicard withouth text are now at the same line again. Thanks to mfilser.
* Rename "Domaine" to "Domain" that is more like English. Thanks to xet7.

[4.16.4]
* Update Wekan to 5.75
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.75)
* Added NodeJS Statistics to Admin Panel/Versio.
* Card detail popup loads now comments if opened from board search.
* Card popup close color remove move bottom delete.
* Comment edit has now a cancel button.
* Checklist and items drag drop scrollable mobile view.

[4.16.5]
* Update Wekan to 5.77
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.76)
* Global search load card details.
* Layout improvement: Adding organisations to the board.
* App reconnect is now possible if the connection was interrupted.
* Boards view has now drag handles at desktop view if drag handles are enabled.
* Account configuration of option loginExpirationInDays is now possible.
* Improve multi selection sidebar opening and closing.

[4.16.6]
* Update Wekan to 5.78
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.78)

[4.16.7]
* Update Wekan to 5.79
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.79)

[4.17.0]
* Update Wekan to 5.80
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.80)
* Show helper at label drag/drop if label popup opened from card details popup. Thanks to mfilser.
* Show or hide members and assignee(s) on minicard. Thanks to Ben0it-T.
* List adding has now a cancel button.
* CustomFields Currency, autofocus on edit.
* Attachments, show file size in KB in card details.
* Sidebar Member Settings Popup has now a Popup title.
* Add copy text button to most textarea fields.

[4.17.1]
* Update Wekan to 5.82
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.82)
* List header contains now a button to add the card to the bottom of the list.

[4.17.2]
* Update Wekan to 5.83
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.83)
* Changed delete checklist dialog to a popup.
* Dragging minicards scrolls now vertically at the end of the screen.

[4.17.3]
* Update Wekan to 5.84
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.84)
* Add full name if exists in email-invite-subject for user to invite.
* Sort Organizations, Teams and People.

[4.17.4]
* Update Wekan to 5.85
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.85)
* Fix mobile card details for Modern Dark theme.
* Fixed undefinded added member to board.

[4.17.5]
* Update Wekan to 5.87
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.87)
* Fix: BoardAdmin can't edit or delete others comments on cards.

[4.17.6]
* Update Wekan to 5.88
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.88)

[4.17.7]
* Update Wekan to 5.90
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.90)
* Edit team/org now update users.teams.teamDisplayName / users.orgs.orgDisplayName.
* Trello api.py: Added for using newest Trello API, to show Trello boards/cards/actions/reactions JSON and download Trello attachments as binary files from S3.
* Trello api.py: Added additional TODO notes.
* Added Info about Shared Templates In Progress.
* Fix getLabels exception in template helper.
* Fixed Templates are Missing, Error: Site not Found "/templates" is missing in the URL.
* Shared Templates part 5: Make visible Create template board checkbox and templates at All Boards page, In Progress.
* Fixed Duplicate board.
* Fix Create Board from Template not opening.

[4.17.8]
* Update Wekan to 5.91
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.91)
* Updated to Node.js v12.22.9.
* Added release scripts for starting and stopping services.
* Updated rebuild-wekan.sh script about installing dependencies.
* Updated dependencies.

[4.17.9]
* Update Wekan to 5.94
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.94)
* Upgraded markdown-it to 12.3.2.
* Added translations: Estonian (Estonia) et_EE, Russian (Ukraine) ru_UA, Ukrainian (Ukraine) uk_UA.
* Fixed OpenAPI docs generating has some swagger error.
* Added copy button to card title.
* Fix Card, List and Comment colors not visible at some themes.

[4.17.10]
* Update Wekan to 5.95
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.95)

[4.17.11]
* Update Wekan to 5.97
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.97)

[4.17.12]
* Update Wekan to 5.98
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.98)

[4.17.13]
* Update Wekan to 5.99
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v5.99)
* Revert rounded corners minicard on moderndark theme.
* Remove not working options from rebuild-wekan.sh.

[4.18.0]
* Update Wekan to 6.00
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.00)
* Adding list select at card details.

[4.18.1]
* Update Wekan to 6.03
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.03)

[4.18.2]
* Update Wekan to 6.05
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.05)
* Fixed Copy card to list does not work, by reverting clientside changes of PR 4333.
* Fixed Problem with selecting action in rule window.
* Fix copy move card.

[4.18.3]
* Update Wekan to 6.07
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.07)
* Fix Forgot Password to be optional.
* Feature/shortcuts for label assignment.
* Feature/propagate OIDC data.
* Global search: Card Details popup opens now in normal view even if maximized card is configured.
* Card details, fix header while scrolling.
* Add subscription to announcements, so that system wide announcements are shown to all.
* Fixed Disable Self-Registration. Added Disable Forgot Password to same Admin Panel page.

[4.18.4]
* Update Wekan to 6.09
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.09)
* Try to allow register and login.
* Try to fix Admin Panel / Disable Registration and Disable Forgot Password.

[4.19.0]
* Update Wekan to 6.11
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.11)
* Update to Nodejs 14.19.0
* Switch from CollectionFS to Meteor-Files.

[4.19.1]
* Update Wekan to 6.12
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.12)
* Feature/empower sso oicd data propagation.
* Add linkedBoard Activities to sidebar.
* Fix Boards.uniqueTitle not working as expected.
* Attachments fixes after migration to meteor files (image preview, global search).

[4.19.2]
* Update Wekan to 6.13
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.14)
* Added Perl scripts for Asana export to WeKan ®.
* Add get list and board cards count to API.
* Added translations: English (Brazil) en-BR and Czech (Czech Republic) cs-CZ.
* Added WRITABLE_PATH to Windows start-wekan.bat.

[4.19.3]
* Update Wekan to 6.15
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.15)
* Show description text on minicard.
* Card Details List-Dropdown is now filled too if the card is opened from global search.
* UI improvements on maximized card header.
* Fix LDAP authentication doesn't support multiple emails in LDAP accounts.
* Added missing characters to Subtasks jade template.

[4.19.4]
* Update Wekan to 6.17
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.17)

[4.19.5]
* Update Wekan to 6.18
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.18)

[4.19.6]
* Update Wekan to 6.19
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.19)

[4.20.0]
* Update Wekan to 6.20
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.20)

[4.21.0]
* Update Wekan to 6.22
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.22)

[4.21.1]
* Update Wekan to 6.24
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.23)
* Fix for https://github.com/wekan/wekan/commit/3ed1fc3e6bdf90ecdc3593468d68a29807ed52b5

[4.21.2]
* Update Wekan to 6.26
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.26)
* Attachment rename.
* Fix Bullets in label selection popup. Related to import nib css reset.
* Fix images not showing correctly, by updating packages like jquery, removing handlebars, changing image attachment view big image popup from swipebox to lightbox, and changing import nib related code.
* Updated to Node.js v14.19.3

[4.21.3]
* Update Wekan to 6.27
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.27)
* Fix opening card scrolls to wrong position when many swimlanes and card at bottom of board.

[4.21.4]
* Update Wekan to 6.28
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.28)

[4.22.0]
* Email display name support
* Optional SSO support

[4.23.0]
* Only disable registration on first run
* Fixup post install message

[4.24.0]
* Update Wekan to 6.29
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.29)

[4.25.0]
* Update Wekan to 6.30
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.30)
* Automatic login with OIDC.
* OIDC/OAuth2 autologin settings for Docker/Snap/Source/Bundle platforms.
* Fix uploading attachments.

[4.25.1]
* Update Wekan to 6.31
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.31)
* Added LaTex support to all input fields with markdown-it-mathjax3. Examples: https://github.com/wekan/wekan/wiki/LaTeX .
* Rescue Save description on card exit. Thanks to Viehlieb.

[4.26.0]
* Update Wekan to 6.33
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.33)
* Add support to validate attachment uploads by type and size.
* Added attachments file type and size snap settings and help text.

[4.27.0]
* Update Wekan to 6.34
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.34)
* Fix and update easysearch

[4.28.0]
* Update Wekan to 6.36
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.36)
* Revert Fix Open card links in current tab. So now links open in new tab.
* Revert Fix URLs to favicons etc for sub-urls, because it broke favicons on subdomain URLs.

[4.28.1]
* Update Wekan to 6.37
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.37)
* Updated dependencies like ostrio:files etc.
* Fix All Boards: The list of lists on each card with the summary counts is not sorted.

[4.28.2]
* Update Wekan to 6.38
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.38)
* Add support to validate attachment uploads by an external program.
* Attachment upload progress bar + multiple files upload.
* Move and copy card dialog remember last selected board.
* Copy card copies now attachments too.
* Copy / move card and checklists using same code.

[4.29.0]
* Update Wekan to 6.39
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.39)
* Add support to validate avatar uploads by type, size and external program.
* Attachment using new feature of Meteor Files 2.3.0.

[4.30.0]
* Update Wekan to 6.40
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.40)
* Move/Copy Card dialog didn't set the last selected board right.

[4.31.0]
* Update Wekan to 6.41
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.41)
* Try to fix EasySearch syntax.

[4.32.0]
* Update Wekan to 6.42
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.42)
* Added translations: English (Malaysia) (en_MY), Japanese (Hiragana) (ja-Hira), Malay (ms)

[4.32.1]
* Update Wekan to 6.43
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.43)
* Revert Remove hard-coded port 8080 binding in Dockerfile.
* Added back autologin, because reverting it broke Google OIDC login.

[4.32.2]
* Update Wekan to 6.44
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.44)
* Add tab view to sidebar members: people, orgs and teams.
* Added missing currentUser.
* Removed old stuff from Dockerfile.
* Fix building Dockerfile on Mac M1 etc.
* Fix 2) Due date is not created nor changed, when cards are moved in the calendar view.

[4.32.3]
* Update Wekan to 6.46
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.46)
* Added limit description on minicard to three lines.
* Added titles to add and edit checklist items.

[4.32.4]
* Update Wekan to 6.49
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.49)
* RegExp possible at Custom Field String Templates.
* Added hide/show to show counters and members on All Boards to Admin Panel.
* Removed Azeri/Azerbaijani from RTL list.
* Checklist copy/move dialog was sometimes empty.a

[4.32.5]
* Update Wekan to 6.50
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.50)
* Added a possibility of getting some WeKan metrics datas.
* Added METRICS_ALLOWED_IP_ADDRESSES settings to Docker/Snap/Source
* https://github.com/wekan/wekan/wiki/Metrics and missing Matomo settings to Snap help.
* Added Romanian translation. Updated translations.
* Fix typos and translate comments to English.
* Build: harden GitHub Workflow permissions.
* Try to fix again Mermaid Diagram error: Maximum call stack size exceeded.
* Show translations debug messages only when DEBUG=true.
* Fix bootstrap and datepicker3 css map missing.

[4.32.6]
* Update Wekan to 6.51
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.51)
* Updated to Node.js v14.20.1

[4.32.7]
* Update Wekan to 6.52
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.52)
* Added faster way to do actions on minicard menu.

[4.32.8]
* Update Wekan to 6.53
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.53)

[4.32.9]
* Update Wekan to 6.54
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.54)

[4.32.10]
* Update Wekan to 6.55
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.55)

[4.32.11]
* Update Wekan to 6.56
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.56)
* Updated to Node.js v14.21.1.
* Updated ostrio:files.
* Updated markdown-it-mermaid.
* Fix Python-Esprima upstream failing.

[4.32.12]
* Update Wekan to 6.59
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.59)

[4.32.13]
* Update Wekan to 6.60
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.60)

[4.32.14]
* Update Wekan to 6.61
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.61)
* Added newuser to api.py.
* For export/print print board/card, added some CSS better. Use browser print preview %20 etc setting to fit to page. Next: Card CSS.
* Upgraded to Meteor 2.9.0.
* Updated to Node.js v14.21.2.
* Updated dependecies like markdown-it-mermaid.
* Update release scripts like Node.js update script.
* Fixed text not visible at white swimlane at themes dark and exodark. Commented out not in use font Poppins.
* Custom fonts were previously removed because they did not work,
* there were errors at browser inspect console.
* Move Desktop Drag Handle setting more right.

[4.32.15]
* Update Wekan to 6.62
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.62)
* Remove duplicate IDs issue about Attachments not visible.
* Fixed installing api2html when generating OpenAPI docs.

[4.33.0]
* Update Wekan to 6.63
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.63)
* Add link card feature to rules.

[4.33.1]
* Update Wekan to 6.64
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.64)
* Fix: changing list color reloads webpage.

[4.33.2]
* Update Wekan to 6.67
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.67)

[4.33.3]
* Update Wekan to 6.68
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.68)
* Upgraded to Meteor 2.9.1.
* Add "use-credentials" directive to site.webmanifest request.
* OIDC login loop for integer user ID. Fix 2.

[4.33.4]
* Update Wekan to 6.69
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.69)
* Updated dependencies

[4.33.5]
* Update Wekan to 6.70
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.70)
* Try to fix User API.

[4.33.6]
* Update Wekan to 6.71
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.71)
* Forked minio npm package to @wekanteam/minio to update package dependencies. S3/MinIO support In Progress.
* Updated GitHub Actions.
* Upgraded to Meteor 2.10.0.
* Fix API Edit card function does nothing.
* Fix Customfields are not added to new cards created with the API.

[4.33.7]
* Update Wekan to 6.72
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.72)

[4.33.8]
* Update Wekan to 6.74
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.74)

[4.33.9]
* Update Wekan to 6.75
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.75)
* Try to fix some security issues
* Fix "Top 10 boards" metric order.
* Swipebox slide background gradient of black to blue, so that back SVG images are visible.

[4.33.10]
* Update Wekan to 6.76
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.76)
* Fix at bottom of list Add button to be higher, so that text Add is not over button borders.

[4.33.11]
* Update Wekan to 6.77
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.77)
* Fixed indentation for image size and compression in docker-compose.yml.
* Made ☰ menu buttons bigger at minicard and list, they were too hard to click when they were small.
* Added "Move card to archive" to minicard ☰ menu.
* Fix attachment migration error about avatarUrl startsWith undefined.
* Try to fix attachment migrations to ostrioFiles, allow existing files to be migrated.

[4.33.12]
* Update Wekan to 6.78
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.78)
* Try to fix some security issue.

[4.33.13]
* Update Wekan to 6.79
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.79)
* Upgraded to Meteor 2.11.0.

[4.33.14]
* Update Wekan to 6.80
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.80)
* Custom Fields, display Grid Button only if more than 1 custom field.

[4.33.15]
* Update Wekan to 6.81
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.81)
* Fix Bug: Card options disappear behind scrollbar in german.
* Add some info about allowed filesizes and filetypes for attachments and avatars.

[4.33.16]
* Update Wekan to 6.82
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.82)
* Fix avatar if Meteor.user() is undefined.
* Fix broken add_board_member API and return value for remove_board_member.
* Try to fix card open position and make card resizeable.

[4.33.17]
* Update Wekan to 6.83
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.83)
* Fix open card position by opening card to fullscreen.

[4.33.18]
* Update Wekan to 6.84
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.84)
* Get card drag/drop working for empty swimlane.
* Added 'next week' due date filter.

[4.33.19]
* Update Wekan to 6.85
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.85)
* Security fix to ReactionBleed in WeKan. It is XSS in feature "Reaction to comment". Thanks to Alexander Starikov
* Disable file validation temporarily, because it causes data loss of some attachments when upgrading. Thanks to xet7.
* Added uploadedAt and copies to be migrated when migrating CollectionFS to ostrio-files. Thanks to xet7
* Added more descriptive times of attachment migrations and uploads. Thanks to xet7.
* Fix LDAP Group Filtering does not work. Thanks to emilburggraf, psteinforth, craig-silva, Daniel-H76, benh57, falkheiland and xet7.
* Save files serverside with filename ObjectID, without filename. Thanks to g-roliveira, iamabrantes, Floaz, koelle25, scott-dunt, mfilser and xet7.
* Fixed count of found cards in Global Search. Thanks to xet7.
* Fix Card opens full width by opening at left. Thanks to xet7.

[4.33.20]
* Update Wekan to 6.86
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.86)

[4.33.21]
* Update Wekan to 6.87
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.87)

[4.34.0]
* Update Wekan to 6.88
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.88)
* Feature: Create Card on Calendar View

[4.34.1]
* Update Wekan to 6.89
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.89)
* Fix for the Create Card at Calendar.
* Fixed broken migration migrate-attachments-collectionFS-to-ostrioFiles.

[4.35.0]
* Update Wekan to 6.90
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.89)
* Added Edit Custom Field Value to api.py.
* To get corrent API docs generated, moved new_checklist_item API to correct file where is other checklist item API.
* Fix sharedDataFolder persisdent provide by k8s has no permission to mkdir and write.

[4.36.0]
* Update Wekan to 6.91
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.91)
* BoardAdmin and Admin can now set board background image URL.

[4.36.1]
* Update Wekan to 6.92
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.92)
* Moved minicard labels from above minicard title to below minicard title.
* Changed Due Date etc dates to be at top of Minicard.

[4.36.2]
* Update Wekan to 6.93
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.93)
* Layout fixes for modern-dark after shuffling labels and date field on minicard.

[4.36.3]
* Update Wekan to 6.95
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.95)
* Revert smaller swimlane height.

[4.36.4]
* Update Wekan to 6.96
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.96)
* This release fixes the following CRITICAL SECURITY ISSUES: Found and fixed more InvisibleBleed of WeKan.

[4.36.5]
* Update Wekan to 6.97
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.97)

[4.36.6]
* Update Wekan to 6.98
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.98)
* Add checklist at top

[4.37.0]
* Update Wekan to 6.99.5
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.99.5)
* Preview PDF.
* Preview PDF translations.
* Preview PDF to have full width, close at top, and improve viewing at mobile.

[4.37.1]
* Update Wekan to 6.99.6
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.99.7)
* Updated dependencies.
* Fix setting background image.
* Added missing character.
* Added back datepicker.

[4.37.2]
* Update Wekan to 6.99.8
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.99.8)
* Fix card creation. Now date fields are checked if they are empty.

[4.37.3]
* Update Wekan to 6.99.9
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v6.99.9)
* Fix "PROPAGATE_OIDC_DATA" mechanism if "info.groups" is undefined.

[4.38.0]
* Update Wekan to 7.01
* Start using OpenID connect
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.01)
* Speed improvements to Board and List loading.
* Forked meteor-globals and meteor-reactive-cache to @wekanteam/meteor-globals and @wekanteam/meteor-reactive-cache to update to newest dependencies.
* Added missing @babel/runtime.

[4.38.1]
* Update Wekan to 7.02
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.02)
* Make swimlane height and list width adjustable.
* Don't translate swimlane height and list width minimum value.
* Only selectively show login elements once settings is loaded.
* First registration after installation must be an admin account.
* Fix `get_board_cards_count`.
* Login layout code cleanup.
* Drag board made translateable.

[4.38.2]
* Update Wekan to 7.03
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.03)
* Fix return value of edit_custom_field_dropdown_item.
* Move authentication UI handling in correct place.

[4.38.3]
* Update Wekan to 7.04
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.04)
* Reduce image size by half
* Fix edit_swimlane.
* Login layout fixes 2.
* Do not open preview for attachments that cannot be previewed.

[4.38.4]
* Update Wekan to 7.05
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.05)
* Thanks to developers of dependencies.
* Make default swimlane auto-height.
* Show option b) -1 for disabling swimlane height at swimlane height popup.
* Fixed Normal (non-admin) board users cannot adjust swimlane height.
* Fixing positioning of opened cards.

[4.38.5]
* Update Wekan to 7.06
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.06)

[4.38.6]
* Update Wekan to 7.07
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.07)
* Fix downloading attachments with unusual filenames.
* Add some filename, if there is no filename after sanitize.

[4.38.7]
* Update Wekan to 7.08
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.08)
* Custom translation strings at Admin Panel.
* Added remaining translations to feature custom translation strings at Admin Panel.
* Corrected source code so that it works correctly with reactiveCache.

[4.38.8]
* Update Wekan to 7.09
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.09)
* Move card to other boards API.
* Upgraded Snap Candidate MongoDB to 6.0.9.
* Fixed building s390x release.
* ReactiveCache, use default parameters.
* ReactiveCache, serialize and parse json with EJSON.
* Translations are working on the client side again.
* ReactiveCache, full implementation of the collection "Translation".
* Attachments, big images are now fully displayed.

[4.39.0]
* Update Wekan to 7.10
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.10)

[4.40.0]
* Update base image to 4.2.0

[4.41.0]
* Update Wekan to 7.12
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.12)
* Fix Add List button too wide at themes: Clearblue, Modern, Exodark.
* Fix Windows build bundle script.

[4.42.0]
* Update Wekan to 7.14
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.14)

[4.43.0]
* Update Wekan to 7.15
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.15)
* Fix missing profile/avatar pictures.
* Commented out links at My Cards Table, because they unexpectly caused to go elsewhere from current view.

[4.43.1]
* Update Wekan to 7.17
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.17)
* Fix move card rule on checklist complete doesn't work, with ReactiveMiniMongoIndex for Server-Side

[4.43.2]
* Update Wekan to 7.18
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.18)
* Added restore list and changing list title to outgoing webhooks. Thanks to gustavengstrom.

[4.43.3]
* Update Wekan to 7.19
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.19)
* Updated swimlane (restore and changed title) and board (changed title) webhooks. Thanks to gustavengstrom.
* Permissions for default board after OIDC/OAuth2 login.

[4.43.4]
* Update Wekan to 7.21
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.21)
* Added standard for public code assessment.
* Upgraded to Meteor 2.14-beta.4.
* Updated dependencies.

[4.43.5]
* Update Wekan to 7.22
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.22)
* At right sidebar, moved Minicard Settings to Card Settings popup.
* New feature: Sidebar / Card Settings / Creator at minicard.

[4.43.6]
* Update Wekan to 7.23
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.23)
* Updated security.md about mitm. Thanks to xet7.
* Upgraded to Meteor 2.14-rc.3. Thanks to Meteor developers.
* Updated dependencies.

[4.43.7]
* Update Wekan to 7.24
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.24)
* Azure AD B2C login using OAuth2.
* Upgrade to Meteor 2.14.

[4.43.8]
* Update Wekan to 7.25
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.25)
* Updated percolate:synced-cron.
* Updated dependencies.
* Translations: Added German (Germany) (de_DE) and Odia (India) (or_IN).
* Fix: Export HTML popup.

[4.43.9]
* Update Wekan to 7.26
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.26)
* Fix some public board buttons.

[4.43.10]
* Update Wekan to 7.27
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.27)
* Fix missing maximize card.

[4.43.11]
* Update Wekan to 7.28
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.28)
* Added archive option to of Wekan API.

[4.43.12]
* Update Wekan to 7.29
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.29)
* Removed markdown-it-mathjax3.
* Updated to Meteor 2.14.1-beta.0

[4.43.13]
* Update Wekan to 7.30
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.30)

[4.44.0]
* Update Wekan to 7.31
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.31)
* api.py: Added getcard and cardsbyswimlane.
* api.py: Add card with label and add a label to a card.
* api.py: Fix BoardID to SwimlaneID in cardsbyswimlane.
* boards.js: New update board title function for API.
* api.py: EDIT BOARD TITLE.

[4.44.1]
* Update Wekan to 7.32
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.32)
* api.py: Added create label.
* api.py: Edit card color.
* api.py: Add checklist with multiple items also or just title.
* api.py: Delete all cards. Will delete all cards from Swimlanes automatically, will retrieve automatically all list id and delete everything.
* cards.js: Added a control to check error if card is not updated.
* Shortcut a to filter by assignees.
* Fixed Error 500 when adding user to a board and multiple same avatar icons by reverting back from Meteor 2.15 to 2.14.

[4.44.2]
* Update Wekan to 7.33
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.33)
* Updated docs for previous version of WeKan.
* OpenAPI: Fix breakage introduced with blank return.

[4.44.3]
* Update Wekan to 7.34
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.34)
* Updated translations

[4.44.4]
* Update Wekan to 7.35
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.35)
* Added back Mathjax that has bug of showing math twice.

[4.44.5]
* Update Wekan to 7.36
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.36)
* api.py: checklistid, checklistinfo, `get_list_cards_count` and `get_board_cards_count`. Thanks to xator91.
* Fixed Card image cover should be margin-top:6px since hamburger menu and due date at the top. Thanks to e-gaulue and xet7.
* Try to fix API get cards wrong order. Please test. Thanks to mohammadZahedian, xator91 and xet7.

[4.44.6]
* Update Wekan to 7.37
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.37)

[4.44.7]
* Update Wekan to 7.38
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.38)
* Fix error during delete.
* Fixed text below custom login logo not visible.
* Fixed In RTL, hamburger needs margin.

[4.45.0]
* Update Wekan to 7.41
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.41)
* Added docs wikis to WeKan repo directory docs, to make possible to send PRs.
* Added script to count lines of code changed per committer email address, because GitHub removed that feature from web UI.
* Add info about GitHub top committers at Finland.
* Clarify usage of api.py.
* Fixed centering of text below custom login logo.
* Fixed In RTL, hamburger margin is too much in mobile.

[4.45.1]
* Update Wekan to 7.42
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.42)
* Fixed CRITICAL SECURITY ISSUE by updating meteor-node-stubs.

[4.45.2]
* Update Wekan to 7.43
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.43)

[4.45.3]
* Update Wekan to 7.44
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.44)
* Collapse Lists.
* [Collapse Swimlanes. In Progress, does not work yet, not visible yet](https://github.com/wekan/wekan/commit b704d58f0f3cf5e7785b79d5a6c9f6c63da4159c).
* Fix board not visible at Collapse Lists.

[4.45.4]
* Update Wekan to 7.45
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.45)
* Fix display of tables with a large number of rows.
* Fix white List color and define Silver in CSS instead of leaving it unset.
* Allow silver color to be set in List and Swimlane.
* Fix Can't set a Due Date that has a leading zero in time, errors with invalid time.

[4.45.5]
* Update Wekan to 7.47
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.47)
* Only show Board name edit button to BoardAdmin.
* Fix Edit Description button is wildly out of place.

[4.45.6]
* Update Wekan to 7.48
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.48)
* Fix Admin Panel pages Organizations and Teams, where HTML Tables were broken.
* Try to show more of title of collapsed list.
* Allow Normal user to add new swimlane, list and label.

[4.45.7]
* Update Wekan to 7.49
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.49)
* Bugfix: Strikethrough inactive Teams and orginizations logic was inverted.
* Changed back to original icon of Edit Description.
* Fill out Org and Team in adminReports.

[4.45.8]
* Update Wekan to 7.50
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.50)

[4.45.9]
* Update Wekan to 7.51
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.51)

[4.45.10]
* Update Wekan to 7.53
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.53)
* Added Dragscroll to scroll the board.
* Updated release script: New install webpage location.
* Updated dependencies.
* Board Menu Popup's were opened twice because of 2 same event creation.
* Fixing of "mode is undefined" on first activity component creation.
* Changing card color now closes the popup and doesn't refresh the whole board page.
* Fix dragscroll package name.
* Reducing card size in database if no planning poker was started.

[4.46.0]
* Update Wekan to 7.54
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.54)
* PWA, move to standalone (standard) to display the status bar
* Added info how with Caddy webserver config, PWA and Android app override icons, showing toolbars, etc.
* Don't set background image in .board-canvas too.
* Bugfix, variable "color" didn't exist.
* Little opacity to minicards to see the background image a little bit.
* Remove attachment storage name from attachment list.
* Attachment popup back was broken since new attachment viewer, now fixed.
* Change Meteor.user() to ReactiveCache.getCurrentUser().
* Fix empty parentId in cards.
* Sidebar xmark icon, add padding and background color.
* Board view, first column a bit smaller to save screen space.

[4.46.1]
* Update Wekan to 7.55
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.55)
* Fix board backgrounds not showing correctly after v7.54.

[4.46.2]
* Update Wekan to 7.56
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.56)
* Updated Browser compatibility matrix.
* Fix ModernDark Mobile View List Titles.

[4.46.3]
* Update Wekan to 7.57
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.57)

[4.46.4]
* Update Wekan to 7.59
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.59)

[4.46.5]
* Update Wekan to 7.60
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.60)

[4.46.6]
* Update Wekan to 7.61
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.61)

[4.47.0]
* Update wekan to 7.62
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.62)
* [Added comment section on card details to avoid loading the card comment activities from the server](https://github.com/wekan/wekan/pull/5566).
* [Checklist items hide per card](https://github.com/wekan/wekan/pull/5567).
* [Checklist multiline insert (many checklist items at once)](https://github.com/wekan/wekan/pull/5568).
* [Each checklist can now be configured to hide checked checklist items](https://github.com/wekan/wekan/pull/5569).
* [Copied updated Docs from wiki to WeKan repo](https://github.com/wekan/wekan/commit/559251eb0d8aea6a714f14224497d0a25c7a3864).
* [Updated docs about Linked Cards](https://github.com/wekan/wekan/commit/96627540da0b6e12890ee1660f4ff0f469bb0e25).

[4.48.0]
* Update wekan to 7.63
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.63)
* [Always close sidebar when user clicks ESC](https://github.com/wekan/wekan/pull/5571).
* [Added 'n' shortcut for adding new minicards to current list](https://github.com/wekan/wekan/pull/5570).
* [Patch to allow shortcuts to work when another keyboard layout is used](https://github.com/wekan/wekan/pull/5574

[4.49.0]
* Update wekan to 7.64
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.64)
* [Add missing semicolon in keyboard.js](https://github.com/wekan/wekan/pull/5580).
* [Make lists fill up space evenly, change listWidth to max-width](https://github.com/wekan/wekan/pull/5581).
* [Change way of disabling scrollbars, disable swimlane scrollbars](https://github.com/wekan/wekan/pull/5583).
* [Improve list auto-width, fix regressions](https://github.com/wekan/wekan/pull/5584).

[4.50.0]
* Update wekan to 7.67
* [Full Changelog](https://github.com/wekan/wekan/pull/5583)
* [Re-introduce list auto-width feature, Reverted scrollbar hiding, Fixed transparent sidebar bug](https://github.com/wekan/wekan/pull/5586).
* [Revert some scrollbar, sidebar and list width changes](https://github.com/wekan/wekan/commit/096fe130f68e0d8d082d309901c75ed04285b7e2).

[4.51.0]
* Update wekan to 7.68
* [Full Changelog](https://github.com/wekan/wekan/commit/096fe130f68e0d8d082d309901c75ed04285b7e2)

[4.52.0]
* Update wekan to 7.69
* [Full Changelog](https://github.com/wekan/wekan/commit/096fe130f68e0d8d082d309901c75ed04285b7e2)
* [Updated dependencies](https://github.com/wekan/wekan/commit/9c87572f90f16fbdddb6a4dff3984e64acac20cc).
* [Fix issue with comments not showing when using Exodark Theme](https://github.com/wekan/wekan/pull/5595).
* [Change archive-card shortcut to backtick for better ergonomics](https://github.com/wekan/wekan/pull/5589).

[4.53.0]
* Update wekan to 7.70
* [Full Changelog](https://github.com/wekan/wekan/pull/5589)
* [Helm Chart: Allow to define securityContext for pod and containers](https://github.com/wekan/charts/pull/37).
* [Move card to archive, add shortcut key  for Persian keyboard](https://github.com/wekan/wekan/commit/80ea1782f935c74f1b7b1fd0fb7700ef9a39dc64).

[4.54.0]
* Update wekan to 7.71
* [Full Changelog](https://github.com/wekan/wekan/pull/5589)
* [To menu right top username, added Support, to have info about from where to get support](https://github.com/wekan/wekan/commit/46327f19a1c6d37f2e5591aa0cc2a882e4c56ee5).

[4.55.0]
* Update wekan to 7.72
* [Full Changelog](https://github.com/wekan/wekan/pull/5589)
* [Optional board list auto-width, Support for min & max width for lists](https://github.com/wekan/wekan/pull/5607).
* [Disabled syncing of old and unrelated docker containers between docker registries](https://github.com/wekan/wekan/commit/17d5fae7bbd96eb6721ad869802cc980c9791c7f).
* [Fix in API user role is not considered](https://github.com/wekan/wekan/commit/c062bd63bbfceb3a96f23ea3e8696534694db54e).

[4.56.0]
* Update wekan to 7.73
* [Changed the default maximum list width](https://github.com/wekan/wekan/pull/5614).
* [Updated Developer Docs about docker compose](https://github.com/wekan/wekan/commit/3e3b629aa2a9efb43b1be8f57009c1d384b66ed8).
* [Hide support popup. It will be made editable later](https://github.com/wekan/wekan/commit/0332ef32980b24a0c4e108436eec5b112287c14b).
* [Hide Accessibility Settings at Admin Panel. It will be continued and added back later](https://github.com/wekan/wekan/commit/e70c51a1f033c8712771238e408cbf52487f07f5).
* [Fix buggy behaviours in board dragscrolling](https://github.com/wekan/wekan/pull/5618).
* [Revert back to have shortcut "c to archive" back for non-Persian keyboards](https://github.com/wekan/wekan/commit/ba0fdaef72393632ca80b42a3c5d2ee5f5e0c76e).
* Hide and disable Keyboard Shortcuts, because they make using Ctrl-C to copy etc impossible.

[4.57.0]
* Update wekan to 7.74
* [Restore keyboard shortcuts, enable per-user toggle, fix Ctrl + C bug by checking the text selection range](https://github.com/wekan/wekan/pull/5628).
* [Fixed keyboard shortcuts defaults, icons and texts to be more understandable](https://github.com/wekan/wekan/commit/955a46ca6016e75c0ac1b01e25f96f47c2844559).
* ["Auto List Width" is now at "List   Set Width" popup](https://github.com/wekan/wekan/commit/a862486ec37fcd022619c7e45ad9ca615aa444ed).
* [Keyboard Shortcuts Enable/Disable is now at Right Sidebar, where already was list of Keyboard Shortcuts](https://github.com/wekan/wekan/commit/275ac445d0cd6f817dd2281aacc27ca7d30b17eb).

[4.58.0]
* Update wekan to 7.76
* [Always handle the escape key when shortcuts are enabled](https://github.com/wekan/wekan/pull/5636).
* [New Swimlane button visible, when there are no swimlanes at all](https://github.com/wekan/wekan/pull/5635).
* [Change margins around keyboard shortcuts toggle to make it clearer, remove old toggle from mobile view](https://github.com/wekan/wekan/pull/5634).
* [Fix Cannot save Layout settings](https://github.com/wekan/wekan/commit/407d018067a5398f0c8d50519096b921d744be68).

[4.59.0]
* Update wekan to 7.77
* [Full changelog](https://github.com/wekan/wekan/releases/tag/v7.77)
* [Allow vertical scrollbars to be disabled (new preference)](https://github.com/wekan/wekan/pull/5643).
* [Enable keyboard shortcuts by default](https://github.com/wekan/wekan/pull/5639).
* [Fix comment backgrounds in cleandark theme](https://github.com/wekan/wekan/pull/5640).
* [Fix weird add checklist buttons in card details](https://github.com/wekan/wekan/pull/5641).
* [Fix "SPACE" shortcut not working after recent fixes](https://github.com/wekan/wekan/pull/5642).

[4.60.0]
* Update wekan to 7.78
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.78)
* [Disable scrollbars on older versions of Chrome and Safari in "no vertical scrollbars" mode](https://github.com/wekan/wekan/pull/5644).
* [Fix styling for vertical scrollbars toggle](https://github.com/wekan/wekan/pull/5645).
* [Add additional archiving keyboard shortcut (added -)](https://github.com/wekan/wekan/pull/5646).
* [Fix assign-self shortcut in shortcut help popup (different from actual shortcut)](https://github.com/wekan/wekan/pull/5647).
* [Fix upper-case keyboard shortcuts & different language shortcuts getting triggered when shortcuts are disabled](https://github.com/wekan/wekan/pull/5648).
* [Fix list header too wide in cleanlight and cleandark themes](https://github.com/wekan/wekan/pull/5649).

[4.61.0]
* Update wekan to 7.79
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.79)
* [Add toggle for week-of-year in date displays (ISO 8601)](https://github.com/wekan/wekan/pull/5652).
* [Assign members using keyboard shortcut Ctrl+Alt+(1-9)](https://github.com/wekan/wekan/pull/5653).

[4.62.0]
* Update wekan to 7.80
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.80)
* [Fix card updating issues with ReactiveCache when using keyboard shortcuts](https://github.com/wekan/wekan/pull/5654).
* [Fix assignee toggling keyboard shortcut to only toggle current board members](https://github.com/wekan/wekan/pull/5655).

[4.63.0]
* Update wekan to 7.81
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.81)
* [Helm Chart: Added secretManaged value for enabling or disabling the creation of secret by Helm](https://github.com/wekan/charts/pull/39).
* [Updated Docker Actions](https://github.com/wekan/wekan/pull/5670).
* [Added Meteor 3.1 learning course to docs](https://github.com/wekan/wekan/commit/0c7e12c5e7f322bdbaaa61100e66153dd0b92e4d).
* [Upgraded to MongoDB 6.0.20 at Snap Candidate](https://github.com/wekan/wekan/commit/b571f1c9530b899db75bf28a03c18277a9b77cb8).
* [Fixed env variable METRICS_ACCEPTED_IP_ADDRESS to be same as at docs](https://github.com/wekan/wekan/commit/0b1e0bd39569175668c195b63dde91bf0e6f1b24).
* [Fixed misspelling of hours at env variable setting LDAP_BACKGROUND_SYNC_INTERVAL](https://github.com/wekan/wekan/commit/36a307785369337a788499065f64175971878930).
* [Helm Chart: Restore pod security context in deployment](https://github.com/wekan/charts/pull/40).

[4.64.0]
* Update wekan to 7.82
* [Full Changelog](https://github.com/wekan/wekan/releases/tag/v7.82)
* [Add possibility to use a token in place of ipaddress to access metrics route](https://github.com/wekan/wekan/pull/5682).
* [Updated dependencies](https://github.com/wekan/wekan/pull/5691).
* [Updated requirements at docs](https://github.com/wekan/wekan/commit/148b81262d0d143460e881d645fefa6740aae40d).
* [Updated dependencies](https://github.com/wekan/wekan/commit/666ee8403388f7d5e1a30cf0e53bc46a70bf1c40).
* [Fixed building WeKan. Updated dompurify. Forked Meteor 2.14 version of meteor-node-stubs to update elliptic](https://github.com/wekan/wekan/commit/18d0fa43275cd2955dd6416213e316ca08a62255).
* [Added missing ) character](https://github.com/wekan/wekan/commit/563a508e269be87eb713e2888409525e1ba82001).

